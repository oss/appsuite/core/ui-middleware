FROM registry.gitlab.open-xchange.com/frontend/dev_env/node_builder:latest AS build-env
LABEL maintainer="ui-team@open-xchange.com"

WORKDIR /app
ADD . /app
RUN pnpm i -s -P --ignore-scripts

FROM gcr.io/distroless/nodejs20-debian12
USER 1000

ARG APP_VERSION
ARG BUILD_TIMESTAMP
ARG CI_COMMIT_SHA
ENV APP_VERSION=$APP_VERSION
ENV BUILD_TIMESTAMP=$BUILD_TIMESTAMP
ENV CI_COMMIT_SHA=$CI_COMMIT_SHA

EXPOSE 8080

COPY --from=build-env /app /app
WORKDIR /app
CMD ["src/index.js"]
