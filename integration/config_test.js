/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { expect } from 'chai'
import { generateSimpleViteManifest, injectApp, mockConfig, mockFetch } from '../spec/util.js'
import * as td from 'testdouble'

describe('Configuration', function () {
  let app
  let config
  let pubClient

  beforeEach(async function () {
    // need to set the redis-prefix. Otherwise, the bull workers will interfere
    process.env.REDIS_PREFIX = Math.random().toString()
    await mockConfig(config = { baseUrls: ['http://ui-server/'] })
    mockFetch({
      'http://ui-server': {
        '/manifest.json': generateSimpleViteManifest({
          'index.html': {}
        }),
        '/index.html': () => new Response('<html><head></head><body>it\'s me</body></html>', { headers: { 'content-type': 'text/html' } }),
        '/meta.json': td.when(td.func()(td.matchers.anything(), td.matchers.anything())).thenReturn(
          new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '2' }), { headers: { 'Content-Type': 'application/json' } })
        )
      }
    })
    await import('../src/redis.js').then(({ switchClient, createClient }) => {
      pubClient = createClient()
      const client = createClient('common client')
      switchClient(client)
      return client.flushdb()
    })
    await import('../src/version.js').then(async ({ updateVersionProcessor }) => {
      await updateVersionProcessor(pubClient)
    })
    app = await injectApp()
  })

  afterEach(async function () {
    td.reset()
    // reset, after the queues were removed
    process.env.REDIS_PREFIX = 'ui-middleware'
  })

  it('updates the configuration when updated on a different node', async function () {
    const response = await app.inject({ url: '/meta' })
    expect(response.json()).to.have.length(2)

    config.baseUrls = []
    await import('../src/version.js').then(async ({ updateVersionProcessor }) => {
      await updateVersionProcessor(pubClient)
      await updateVersionProcessor(pubClient)
    })

    const response2 = await app.inject({ url: '/meta' })
    expect(response2.json()).to.have.length(1)
  })
})
