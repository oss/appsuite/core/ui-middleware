/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { expect } from 'chai'
import { generateSimpleViteManifest, injectApp, mockConfig, mockFetch } from '../spec/util.js'
import * as td from 'testdouble'
import { getRedisKey } from '../src/util.js'

describe('Updates the version', function () {
  let app
  let pubClient
  let runUpdate

  beforeEach(async function () {
    // need to set the redis-prefix. Otherwise, the bull workers will interfere
    process.env.REDIS_PREFIX = Math.random().toString()
    await mockConfig({ baseUrls: ['http://ui-server/'] })
    mockFetch({
      'http://ui-server': {
        '/manifest.json': generateSimpleViteManifest({
          'index.html': {}
        }),
        '/index.html': () => new Response('<html><head></head><body>it\'s me</body></html>', { headers: { 'content-type': 'text/html' } }),
        '/meta.json': td.when(td.func()(td.matchers.anything(), td.matchers.anything())).thenReturn(
          new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '2' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '2' }), { headers: { 'Content-Type': 'application/json' } }),
          new Response(JSON.stringify({ commitSha: '2' }), { headers: { 'Content-Type': 'application/json' } })
        )
      }
    })
    await import('../src/redis.js').then(({ switchClient, createClient }) => {
      pubClient = createClient('pubClient')
      const client = createClient('common client')
      switchClient(client)
      return client.flushdb()
    })
    await import('../src/version.js').then(async ({ updateVersionProcessor }) => {
      runUpdate = updateVersionProcessor
      await runUpdate(pubClient)
      return runUpdate(pubClient)
    })
    app = await injectApp()
  })

  afterEach(async function () {
    td.reset()
    // reset, after the queues were removed
    process.env.REDIS_PREFIX = 'ui-middleware'
  })

  it('with manually triggered job', async function () {
    const responseBeforeUpdate = await app.inject({ url: '/index.html' })
    expect(responseBeforeUpdate.statusCode).to.equal(200)
    expect(responseBeforeUpdate.headers.version).to.equal('85101541')

    await runUpdate(pubClient)
    await runUpdate(pubClient)
    const responseAfterUpdate = await app.inject({ url: '/index.html' })
    expect(responseAfterUpdate.statusCode).to.equal(200)
    expect(responseAfterUpdate.headers.version).to.equal('85102502')
  })

  it('with automatically triggered job', async function () {
    const subClient = await import('../src/redis.js').then(({ createClient }) => createClient('subClient'))
    const responseBeforeUpdate = await app.inject({ url: '/index.html' })
    expect(responseBeforeUpdate.statusCode).to.equal(200)
    expect(responseBeforeUpdate.headers.version).to.equal('85101541')

    // wait for the update event to happen
    await new Promise(resolve => {
      const key = getRedisKey({ name: 'updateVersionInfo' })
      subClient.subscribe(key)
      subClient.on('message', async (channel, version) => {
        if (channel !== key) return
        resolve()
      })
      runUpdate(pubClient).then(() => runUpdate(pubClient))
    })

    const responseAfterUpdate = await app.inject({ url: '/index.html' })
    expect(responseAfterUpdate.statusCode).to.equal(200)
    expect(responseAfterUpdate.headers.version).to.equal('85102502')
  })

  describe('with initial version', function () {
    beforeEach(async function () {
      td.reset()
      // need to set the redis-prefix. Otherwise, the bull workers will interfere
      process.env.REDIS_PREFIX = Math.random().toString()
      await mockConfig({ baseUrls: ['http://ui-server/'] })
      mockFetch({
        'http://ui-server': {
          '/manifest.json': generateSimpleViteManifest({
            'index.html': {}
          }),
          '/index.html': () => new Response('<html><head></head><body>it\'s me</body></html>', { headers: { 'content-type': 'text/html' } }),
          '/meta.json': td.when(td.func()(td.matchers.anything())).thenReturn(
            new Response(JSON.stringify({ commitSha: '1' }), { headers: { 'Content-Type': 'application/json' } }),
            new Response(JSON.stringify({ commitSha: '2' }), { headers: { 'Content-Type': 'application/json' } })
          )
        }
      })

      const { switchClient, createClient } = await import('../src/redis.js')
      const client = createClient('common client')
      switchClient(client)
      await client.flushdb()
      // preconfigure redis
      await Promise.all([
        client.set(getRedisKey({ name: 'versionInfo' }), JSON.stringify({ version: '12345' })),
        client.set(getRedisKey({ version: '12345', name: '/index.html' }) + ':body', '<html><head></head><body>it\'s me</body></html>'),
        client.set(getRedisKey({ version: '12345', name: '/index.html' }) + ':meta', JSON.stringify({ headers: { 'content-type': 'text/html', version: '12345' } }))
      ])
      app = await injectApp()
    })

    it('uses version from redis if present', async function () {
      app = await injectApp()

      const response = await app.inject({ url: '/index.html' })
      expect(response.statusCode).to.equal(200)
      expect(response.headers.version).to.equal('12345')
    })
  })
})
