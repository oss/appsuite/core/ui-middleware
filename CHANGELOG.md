# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.1.1] - 2024-09-27

### Changed

- dependency updates


## [2.1.0] - 2024-09-10

### Added

- Added ajv-formats for migration [`8926d58`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/8926d58e87c2777424aeefbe6b662ad0198e1197)
- Manually orchestrate version upgrades [`5c75653`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/5c75653f53d727c31740487baa1c963b81ff9dcc)
    - Use PROPAGATE_UPGRADES env variable to defer update events to core-ui-mw pods
    - Set to `false` to only warm up cache
    - Re-deploy with set to `true` to actually roll out the upgrade

### Changed

- Set brotli compression quality to 5 [`44623c9`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/44623c9d3a1d3c25c78a2b55f01d4b49a24c5fd5)

### Fixed

- Helm: Support to use {{ .Release.Name }} in existingConfigMap value [`45bbc11`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/45bbc115aebd44021edbfc500f35bb6806d20a8e)


## [2.0.4] - 2024-04-05

### Added

- Improve 404 error message in HTML mode

### Fixed

- Copy & paste error in helm chart secret template [`03e26f5`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/03e26f59c10c4174ea3ab50fd2f1082ce061b376)

## [2.0.3] - 2024-04-02

### Added

- Support TLS mode for redis client [`f44c8d4`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/f44c8d4182f682b30e0f92734ae595231ad32983)

### Changed

- Helm: Make security context configuration overwritable [`edeaaa8`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/edeaaa803bbd9f161da92a8e5600ba4e44c4363f)

### Removed

- Unused service account [`f7d1f63`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/f7d1f6374e5132e0d3eb0ed8729346763ee89d85)

## [2.0.2] - 2024-02-01

### Fixed

- [`OXUIB-2698`](https://jira.open-xchange.com/browse/OXUIB-2698): SSRF/XSS: Prevent to cache resources from external origins [`e254e46`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/e254e461054e28a1f277074084760daf6f747635)

## [2.0.1] - 2024-01-29

### Added

- Add support to set cpu/memory resources for updater [`a93fa11`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/a93fa1172b7cd9e1ee8f794f6af919f45c8f1b49)

### Fixed

- [`OXUIB-2698`](https://jira.open-xchange.com/browse/OXUIB-2698): SSRF/XSS: Prevent to cache resources from external origins [`d3bc298`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/d3bc298f8a95084af69a51f660c7b42b993ece43)

## [2.0.0] - 2023-10-27

### Added

- Gauge for Redis version [`a738be8`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/a738be849f7716d719d99369d746966c4c2b05c6)
- Logging Endpoint for identifying code loading issues [`70967e3`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/70967e3566f51443821682eb961005caeeeed84e)
- More log output for possible file serving errors [`1fa1aaa`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/1fa1aaaf79293490eb3335dd67535e3fdc501a0a)
- Redis memory gauge [`d03809c`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/d03809c8519980146678134b3350d92e6cd89bce)

### Changed

- [`OXUI-1345`](https://jira.open-xchange.com/browse/OXUI-1345): Refactor cache handling [`a1495f4`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/a1495f40cf79b2a59ac8878f27fb8bbabeb9ac41)
  - Introduce two separate roles for middleware deployments:
    - Ui-middleware: read from redis only, listen to version updates, can easily be scaled
    - Updater: split out all code that writes to redis, no need to scale &gt; 1
- **Breaking change:** Redis Configuration [`f25cf88`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/f25cf886b59fcebd412535406b452c47b601cde2)
  - Redis is now mandatory and enabled by default
  - Added Sentinel, Standalone and Cluster support
  - Standalone mode with sidecar is used by default
    - If no host(s) are configured, a redis sidecar is injected into the deployment
    - No extra configuration needed
    - This is intended for development and does not scale.
  - Helm values have changed, see README.md:
    - `Redis.host` has been renamed to `redis.hosts` and expects a list of strings with the following pattern: &lt;localhost:6379&gt; (needed for sentinel and cluster modes)
    - `Redis.port` has been removed, see `redis.hosts`
- Error handling improved [`aa43d2a`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/aa43d2a50fc5e0629b7a1b6326ffddbbf706d848)
- Explicitly set content-type to utf-8 for javascript files [`e785c24`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/e785c241ec96296f1be564c9515691fc774f3b80)
- Ingress is now disabled by default [`e8c6664`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/e8c6664ace9db8aa89f79f42a1a94822a75b3c0e)
- Reconnect common redis client if redis is not reachable [`1e391b4`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/1e391b4582e15ab8ea386babfbf89222210b0df5)
- Renamed monitoring port to follow Istio naming convention [`10876a1`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/10876a1435e40656be24138af90838532e4e6b7f)
- Show cache content information on default ui-mw dashboard [`c97b2e9`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/c97b2e925994698739fd72f11028eb0865fbbf01)
- UI Middleware is published under AGPL 3 [`0cc3886`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/0cc388647a87029d8a8e9ddc03de767c39b9c944)
- Update to node 20 [`3ec46f4`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/3ec46f43603641fa934d0dfd0692c8bf7b11bcb9)

### Fixed

- [`OXUI-1330`](https://jira.open-xchange.com/browse/OXUI-1330): Log aggregate errors seperately [`dfbe69a`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/dfbe69afbc3a1f510a240ed3a9deef587638f03b)
- Faulty routing when running on different app root [`d647697`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/d647697c8ee2bc9d3d271ed9aa9973ab3c4fb6b7)
- Monitoring port name for istio to pick things up [`adab68d`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/adab68d11c3ca4b2579cf02e755b4bcd55e67026)

## [1.8.0] - 2023-04-06

### Added

- Webmanifest (pwa.json) endpoint [`eaaf802`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/eaaf8021ef65ec2f43a3483b6c617f99743b5308)

## [1.7.0] - 2023-02-10

### Added

- [`OXUI-1118`](https://jira.open-xchange.com/browse/OXUI-1118) - Integrate grafana dashboard [`d40758e`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/d40758e00ea4d2b4cf845278e20f9446b420e04f)

## [1.6.0] - 2022-12-16

### Added

- check for version-mismatches when ui-containers provide a version header [`4d22e4a`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/4d22e4aee70a425d7b7c318a677011bde7fa6aad)
- [`OXUI-1092`](https://jira.open-xchange.com/browse/OXUI-1092) - Add configurable salt to manually invalidate client caches [`cd358ee`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/cd358ee87f357d718c2fbbb0eab3d26807bebd81)
- ui-middleware will slowly warm it's caches after version updates [`3e0419d`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/3e0419d90d3d441994c7aa76b037ecda00e3ceb5)

### Changed

- Update helm chart to autoscaling/v2 [`a2aed27`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/a2aed2723c8b6782f62b5655ed35402cded16c7b)

### Removed

- Code injection to load stylesheets for js files [`50ceed1`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/50ceed1f4ca2083c3ef5c73a99c03f00ba2837d6)
  - Injection of code to load stylesheets is now required to be resolved during
      the build process of ui projects.
      The plugin vite-plugin-ox-css is available.

### Fixed

- [`OXUIB-2001`](https://jira.open-xchange.com/browse/OXUIB-2001) - Service doesn't recover from missing Redis service [`73d3ec2`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/73d3ec204829dcffba4520293e747bdd5d5a8741)
- Cannot run ui-middleware without redis [`3585eed`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/3585eede9a500246f49206356a64d801bf8804c4)

## [1.5.1] - 2022-11-18

### Fixed

- updateVersionProcessor using wrong signature of getViteManifests [`771290b`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/771290b26517013cfaa8b8baad129d710fb14ad6)

## [1.5.0] - 2022-11-18

### Added

- [`OXUI-1083`](https://jira.open-xchange.com/browse/OXUI-1083) - Support for brotli compression [`0dd117f`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/0dd117fb26aa8169a806e0a43415e4b90464e07e)
- First dedicated metrics to analyse cache behaviour [`4435e28`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/4435e2829da76a1e3acb09278105879db0d8ee9b)

### Changed

- Use dedicated port for /metrics endpoints [`e3a0de0`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/e3a0de0b59a8c485fdd2f41ca1d720d8576f08f8)

## [1.4.2] - 2022-11-11

### Added

- Documentation about architecture [`d82ed0d`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/d82ed0db05fb679e09751450585d5297873c9f30)

### Fixed

- [`OXUIB-1961`](https://jira.open-xchange.com/browse/OXUIB-1961): UI Middleware throwing "file not found exception" and not serving recent versions [`9dbf03d`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/9dbf03d0bda80b4e07037f299f72ceff713cbe04)

## [1.4.1] - 2022-09-13

### Fixed

- UI-Middleware responds with a 404 when the trailing slash is not found [`1690559`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/16905598879cb9a706c7cdfbedc9270baf9b0ab8)

## [1.4.0] - 2022-09-09

### Added

- [`OXUI-1015`](https://jira.open-xchange.com/browse/OXUI-1015) Lower initialDelaySeconds for readiness check so that nodes become ready faster
- ui-files are compressed before stored in cache or delivered to the client [`51ad5f9`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/51ad5f9adea717a49202baca767a743d5a4806cb)
- UI-Version update checks are only executed, if two subsequent versions don't change [`fd3764d`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/fd3764d7bb6609e6626b0f9781bc48b88e777a58)
- Improved logging

### Changed

- Replace express with fastify to improve overall performance [`073b611`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/073b611d1d1351aa8455dfa965c154955549c416)
- Access log level is now 'debug' instead of 'info' [`739037e`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/739037e021e62fe5b335569792cd1564cebc17b9)

### Fixed

- Fix error handling in case the UI-containers are not accessible [`739037e`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/739037e021e62fe5b335569792cd1564cebc17b9)

## [1.3.8] - 2022-07-20

### Fixed

- file logging does not contain the requested file version [`70e2686`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/70e2686498de3f608af8de73adff5399e513e8ae)

## [1.3.7] - 2022-07-19

### Added

- Add extended logging for file-caching mechanics [`9354abe`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/9354abe408a4b8a828611908ebc9dc0de363eac6)

## [1.3.6] - 2022-07-11

### Added

- Add extended logging to some modules [`6906d7f`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/6906d7f213f67324cf0f609d56d47dd4d6d15142)
- Add logging to the version check functions on log level 'info' and 'debug'
- Add some time measures on ready checks to see what's taking so long on startup

### Changed

- Changed all 'trace' logs to level 'debug'

## [1.3.5] - 2022-06-15

### Fixed

- Release toolchain

## [1.3.4] - 2022-05-30

### Added

- Add trace logging for the cache [`c4fcec5`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/c4fcec5f90f10de02e22e03736774afa8f51215f)

### Fixed

- [`OXUIB-1631`](https://jira.open-xchange.com/browse/OXUIB-1631) memory spikes immediately after startup [`43b4dd6`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/43b4dd6a6d3964123cff02bf7c55755fa3f9e058) [`c9d89ba`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/c9d89ba0c41c9e927b9e543b042e1fe4f93489ff) [`90e3c32`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/90e3c325e52b274273e8d8eba57b3947453c9a1b)

## [1.3.3] - 2022-05-10

### Fixed

- [`OXUIB-1632`](https://jira.open-xchange.com/browse/OXUIB-1632) Multiple ui-middleware-nodes might end up in different configurations [`1ee3761`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/1ee376117a8468a267f2f2297694fc4d9003458f)

## [1.3.2] - 2022-05-05

### Fixed

- [`OXUIB-1621`)`](https://jira.open-xchange.com/browse/OXUIB-1621) /meta does not work with disabled redis [`0131ccb`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/0131ccb563d06ccfc91574b0d8eedd866fac98f0)

## [1.3.1] - 2022-05-04

### Added

- Add optional flag for redis configuration [`1eeca35`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/1eeca35683ba57301ba23c21d721e2b160d7f262)

## [1.3.0] - 2022-05-04

### Added

- Redis-based cache to allow scalability [`8c921c8`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/8c921c8f7da44d59ec9a3a098cb6c3579bcdb840)

## [1.2.1] - 2022-04-05

### Changed

- **Breaking change:** unify name prefix of metrics exposed using promclient [`ae56d8c`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/ae56d8c0eb18e2aefd2938291616ed1ac24dc366)

### Fixed

- [`OXUIB-1528`](https://jira.open-xchange.com/browse/OXUIB-1528): Read-through cache not working [`ba11d96`](https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/commit/ba11d966ff86b2f1dc2611e1be7f4ceb1ad3f310)

## [1.2.0] - 2022-03-28

### Changed

- Improve version detection

### Fixed

- /ready might trigger warmup multiple times

## [1.1.236709] - 2022-03-03

### Fixed

- OXUIB-1397 - Parent or opener do not reference opening window

## [1.1.236708] - 2022-03-01

### Fixed

- OXUIB-1378 - 7.10.x share links don't work with 8.0
- more sane default for memory limits
- version hash in scaled environments
- OXUIB-1380 - UI forever (enough) broken after update

### Added

- redirect endpoint for login redirects

## [1.1.236707] - 2022-02-10

- Add release workflow

## [1.1.236706]

### Added

- Start changelog

[unreleased]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.1.1...main
[1.3.5]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.3...1.3.5
[1.3.4]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.3...1.3.4
[1.3.3]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.2...1.3.3
[1.3.2]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.1...1.3.2
[1.3.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.2.1...1.3.0
[1.2.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.1.236709...1.2.0
[1.1.236709]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.1.236708...1.1.236709
[1.1.236708]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.1.236707...1.1.236708
[1.1.236707]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.1.0...1.1.236707

[2.1.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.1.0...2.1.1
[2.1.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.0.4...2.1.0
[2.0.4]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.0.3...2.0.4
[2.0.3]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.0.2...2.0.3
[2.0.2]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.0.1...2.0.2
[2.0.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.8.0...2.0.0
[1.8.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.7.0...1.8.0
[1.7.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.6.0...1.7.0
[1.6.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.5.1...1.6.0
[1.5.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.5.0...1.5.1
[1.5.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.4.2...1.5.0
[1.4.2]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.4.1...1.4.2
[1.4.1]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.4.0...1.4.1
[1.4.0]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/list...1.4.0
[1.3.8]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.7...1.3.8
[1.3.7]: https://gitlab.open-xchange.com/appsuite/web-foundation/ui-middleware/compare/1.3.6...1.3.7
