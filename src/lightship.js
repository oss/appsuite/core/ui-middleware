/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { createLightship } from 'lightship'
import logger from './logger.js'

const lightship = await createLightship({
  port: Number(process.env.LIGHTSHIP_PORT)
})

// This is a graceful shutdown handler in case of uncaught exceptions
process.on('uncaughtException', async err => {
  logger.error(err, 'uncaughtException')
  await lightship.shutdown()
})

// This is a graceful shutdown handler in case of unhandled promise rejections
process.on('unhandledRejection', async err => {
  logger.error(err, 'unhandledRejection')
  await lightship.shutdown()
})

// This is a graceful shutdown handler in case of SIGINT
process.on('SIGINT', async (process) => {
  logger.info('SIGINT received')
  await lightship.shutdown()
})

export default lightship
