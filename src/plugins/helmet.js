/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import helmet from '@fastify/helmet'
import fp from 'fastify-plugin'

/**
 * @fastify/helmet enables the use of helmet in a Fastify application.
 *
 * Helmet helps you secure your Fastify apps by setting various HTTP headers.
 * It's not a silver bullet, but it can help!
 *
 * Options can be set in the second parameter of the plugin registration.
 *
 * @see https://github.com/fastify/fastify-helmet
 */

export default fp(async (fastify) => {
  fastify.register(helmet, {
    contentSecurityPolicy: false,
    crossOriginEmbedderPolicy: false,
    originAgentCluster: false,
    crossOriginOpenerPolicy: { policy: 'same-origin-allow-popups' }
  })
})
