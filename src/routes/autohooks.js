/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { getLatestVersion } from '../version.js'
export default async function (app, opts) {
  // Add a hook to set the version headers
  app.addHook('preHandler', async (req, reply) => {
    const latestVersion = await getLatestVersion()
    const version = req.headers.version || latestVersion
    reply.header('version', version)
    reply.header('latest-version', latestVersion)
    reply.version = version
    if (req.body) req.log.trace({ body: req.body }, 'parsed body')
  })

  // Add a hook to log errors
  app.addHook('onError', (req, reply, fastifyError, done) => {
    // @ts-ignore - AggregateError is not yet part of the types
    const aggregatedErrors = fastifyError.errors instanceof Array ? fastifyError.errors : [fastifyError]
    aggregatedErrors.forEach(error => error.err ? reply.log.error(error, error.err.message) : reply.log.error(error, error.message || 'request errored'))
    done()
  })

  const slowRequestThreshold = parseInt(process.env.SLOW_REQUEST_THRESHOLD) || 4000

  // Logs the request with the 'debug' level and also logs headers with the 'trace' level
  app.addHook('onResponse', (req, reply, done) => {
    const responseTime = reply.elapsedTime
    const loggingOptions = { url: req.raw.url, res: reply, method: req.method, responseTime }
    /* c8 ignore next */
    if (process.env.LOG_LEVEL === 'trace') loggingOptions.headers = req.headers
    reply.log.debug(loggingOptions, 'request completed')
    if (responseTime >= slowRequestThreshold) {
      reply.log.info(loggingOptions, 'slow request')
    }
    done()
  })
}
