/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { config } from 'dotenv-defaults'
import pino from 'pino'

config()

const logger = {
  base: undefined, // Omits pid and hostname
  redact: {
    paths: ['headers.authorization', 'headers.cookie', 'headers.host', 'key', 'password', 'salt', 'hash'],
    censor: '**REDACTED**',
    remove: true
  },
  level: process.env.LOG_LEVEL,
  timestamp: () => `,"timestamp":${Date.now()}`,
  formatters: {
    level (label, number) {
      // See: https://github.com/pinojs/pino/blob/master/docs/api.md#logger-level
      // Also: https://confluence.open-xchange.com/pages/viewpage.action?spaceKey=EN&title=Logging
      switch (number) {
        case 10: // trace
          return { level: 8 }
        case 20: // debug
          return { level: 7 }
        case 30: // info
          return { level: 6 }
        case 40: // warn
          return { level: 4 }
        case 50: // error
          return { level: 3 }
        case 60: // fatal
          return { level: 0 }
      }
    }
  }
}

export default pino(logger)
