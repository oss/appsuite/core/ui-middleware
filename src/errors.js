/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

export class NotFoundError extends Error {
  constructor (message, options = {}) {
    super(message, options)
    this.status = options.status
  }
}

export class VersionMismatchError extends Error {}

export class NotAllowedOriginError extends Error {
  constructor (message, options = {}) {
    super(message, options)
    this.status = options.status
  }
}

/**
 * Returns true, if the error is a VersionMismatchError or if the error is an aggregate error containing a VersionMismatchError
 * @param {AggregateError | Error} err
 * @return boolean
 */
export function isVersionMismatchError (err) {
  const errors = err instanceof AggregateError ? err.errors : [err]
  return errors.some(error => error instanceof VersionMismatchError)
}

/**
 * Returns true, if the error is a NotFoundError or if the error is an aggregate error containing a NotFoundError
 * @param {AggregateError | Error} err
 * @return boolean
 */
export function isNotFoundError (err) {
  const errors = err instanceof AggregateError ? err.errors : [err]
  return errors.some(error => error instanceof NotFoundError)
}

export function isNotAllowedOriginError (err) {
  const errors = err instanceof AggregateError ? err.errors : [err]
  return errors.some(error => error instanceof NotAllowedOriginError)
}
