/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { decompressBrotli, generateSimpleViteManifest, injectApp, mockConfig, mockFetch, mockRedis, wait } from './util.js'
import { expect } from 'chai'
import * as td from 'testdouble'
import RedisMock from 'ioredis-mock'

describe('UI Middleware', function () {
  let app
  let fetchConfig

  beforeEach(async function () {
    await mockConfig({ baseUrls: ['http://ui-server/'] })
    await mockRedis()
    mockFetch(fetchConfig = {
      'http://ui-server': {
        '/manifest.json': generateSimpleViteManifest({ 'example.js': 'test' }),
        '/example.js': ''
      }
    })
    app = await injectApp()
  })

  afterEach(async function () {
    td.reset()
    await new RedisMock().flushdb()
  })

  it('fetches manifest data', async function () {
    const response = await app.inject({ url: '/manifests' })
    const body = await decompressBrotli(response.rawPayload)
    expect(response.statusCode).to.equal(200)
    expect(response.headers['content-encoding']).to.equal('br')
    expect(body).to.deep.equal([{ namespace: 'test', path: 'example' }])
  })

  it('caches manifest data when configuration changes', async function () {
    const response = await app.inject({ url: '/manifests' })
    const body = await decompressBrotli(response.rawPayload)
    expect(response.statusCode).to.equal(200)
    expect(response.headers['content-encoding']).to.equal('br')
    expect(body).to.deep.equal([{ namespace: 'test', path: 'example' }])

    fetchConfig['http://ui-server'] = {
      '/manifest.json': generateSimpleViteManifest({ 'example.js': 'other' }),
      '/example.js': ''
    }

    await wait(150)

    const response2 = await app.inject({ url: '/manifests' })
    const body2 = await decompressBrotli(response.rawPayload)
    expect(response2.statusCode).to.equal(200)
    expect(response2.headers['content-encoding']).to.equal('br')
    expect(body2).to.deep.equal([{ namespace: 'test', path: 'example' }])
  })

  describe('multiple configurations', function () {
    beforeEach(async function () {
      await mockConfig({ baseUrls: ['http://ui-server/', 'http://ui-server2/'] })
      fetchConfig['http://ui-server2'] = {
        '/manifest.json': generateSimpleViteManifest({ 'example2.js': 'thing' }),
        '/example2.js': ''
      }
      app = await injectApp()
    })

    it('can load multiple configurations', async function () {
      const response = await app.inject({ url: '/manifests' })
      const body = await decompressBrotli(response.rawPayload)
      expect(response.statusCode).to.equal(200)
      expect(response.headers['content-encoding']).to.equal('br')
      expect(body).to.deep.equal([
        { namespace: 'test', path: 'example' },
        { namespace: 'thing', path: 'example2' }
      ])
    })
  })
})
