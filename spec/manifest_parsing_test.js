/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { viteToOxManifest } from '../src/util.js'
import { expect } from 'chai'

describe('Vite manifest parsing', function () {
  it('should work', function () {
    const manifests = viteToOxManifest({
      '../io.ox/guidedtours/i18n.de_DE.js': {
        file: 'io.ox/guidedtours/i18n.de_DE.js',
        src: '../io.ox/guidedtours/i18n.de_DE.js',
        isEntry: true,
        meta: {}
      },
      '../io.ox/guidedtours/i18n': {
        file: 'io.ox/guidedtours/i18n.3de05d46.js',
        src: '../io.ox/guidedtours/i18n',
        isEntry: true,
        imports: [
          '_preload-helper-a7bbbf37.js'
        ],
        meta: {
          gettext: {
            dictionary: true
          },
          manifests: [
            {
              namespace: 'i18n'
            }
          ]
        }
      },
      'io.ox/guidedtours/intro.js': {
        file: 'io.ox/guidedtours/intro.e84819a5.js',
        src: 'io.ox/guidedtours/intro.js',
        isEntry: true,
        isDynamicEntry: true,
        imports: [
          '../io.ox/guidedtours/i18n',
          '_preload-helper-a7bbbf37.js'
        ],
        meta: {}
      },
      'io.ox/guidedtours/main.js': {
        file: 'io.ox/guidedtours/main.07676e21.js',
        src: 'io.ox/guidedtours/main.js',
        isEntry: true,
        imports: [
          '_preload-helper-a7bbbf37.js',
          '../io.ox/guidedtours/i18n'
        ],
        dynamicImports: [
          'io.ox/guidedtours/intro.js',
          'io.ox/guidedtours/multifactor.js'
        ],
        meta: {
          manifests: [
            {
              namespace: 'settings'
            },
            {
              namespace: 'io.ox/core/main',
              title: 'Guided tours',
              company: 'Open-Xchange',
              icon: '/images/icon.png',
              category: 'Dev',
              settings: false,
              index: 100,
              package: 'open-xchange-guidedtours'
            }
          ]
        }
      },
      'io.ox/guidedtours/multifactor.js': {
        file: 'io.ox/guidedtours/multifactor.22d3e17d.js',
        src: 'io.ox/guidedtours/multifactor.js',
        isEntry: true,
        isDynamicEntry: true,
        imports: [
          '_preload-helper-a7bbbf37.js',
          '../io.ox/guidedtours/i18n',
          'io.ox/guidedtours/main.js'
        ],
        meta: {}
      },
      'io.ox/guidedtours/utils.js': {
        file: 'io.ox/guidedtours/utils.91ad511f.js',
        src: 'io.ox/guidedtours/utils.js',
        isEntry: true,
        imports: [
          '_preload-helper-a7bbbf37.js'
        ],
        meta: {}
      },
      '_preload-helper-a7bbbf37.js': {
        file: 'io.ox/guidedtours/preload-helper-a7bbbf37.js'
      }
    })
    expect(Array.isArray(manifests)).to.equal(true)
    expect(manifests.length).to.equal(3)
    expect(manifests.map(manifest => manifest.path)).to.deep.equal([
      'io.ox/guidedtours/i18n.3de05d46',
      'io.ox/guidedtours/main.07676e21',
      'io.ox/guidedtours/main.07676e21'
    ])
    expect(manifests.map(manifest => manifest.namespace)).to.deep.equal([
      'i18n',
      'settings',
      'io.ox/core/main'
    ])
    expect(manifests.map(manifest => manifest.dependencies)).to.deep.equal([
      ['io.ox/guidedtours/preload-helper-a7bbbf37.js'],
      ['io.ox/guidedtours/preload-helper-a7bbbf37.js', 'io.ox/guidedtours/i18n.3de05d46.js'],
      ['io.ox/guidedtours/preload-helper-a7bbbf37.js', 'io.ox/guidedtours/i18n.3de05d46.js']
    ])
  })
})
