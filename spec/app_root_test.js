/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { injectApp, mockRedis, mockFetch, mockConfig } from './util.js'
import { expect } from 'chai'
import * as td from 'testdouble'
import RedisMock from 'ioredis-mock'

describe('With different app root', function () {
  let app

  beforeEach(async function () {
    await mockConfig({ baseUrls: ['http://ui-server/'] })
    const { client } = await mockRedis()
    mockFetch({
      'http://ui-server': {
        '/favicon.ico': 'not really a favicon, though'
      }
    })
    await Promise.all([
      client.set('ui-middleware:viteManifests', JSON.stringify({})),
      client.set('ui-middleware:/example.js:body', 'this is example'),
      client.set('ui-middleware:/example.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript' } })),
      client.set('ui-middleware:/test.txt:body', 'this is test'),
      client.set('ui-middleware:/test.txt:meta', JSON.stringify({ headers: { 'content-type': 'text/plain' } })),
      client.set('ui-middleware:/index.html:body', '<html><head></head><body>it\'s me</body></html>'),
      client.set('ui-middleware:/index.html:meta', JSON.stringify({ headers: { 'content-type': 'text/html' } }))
    ])
    app = await injectApp('/appsuite/')
  })

  afterEach(async function () {
    process.env.APP_ROOT = '/'
    await new RedisMock().flushdb()
    td.reset()
  })

  it('serves files defined in manifest.json file', async function () {
    const response = await app.inject({ url: '/appsuite/example.js' })
    expect(response.statusCode).to.equal(200)
    expect(response.headers['content-type']).to.equal('application/javascript')
    expect(response.body).to.equal('this is example')

    const response2 = await app.inject({ url: '/appsuite/test.txt' })
    expect(response2.statusCode).to.equal(200)
    expect(response2.headers['content-type']).to.equal('text/plain')
    expect(response2.body).to.equal('this is test')
  })

  it('serves / as index.html', async function () {
    const response = await app.inject({ url: '/appsuite/' })
    expect(response.statusCode).to.equal(200)
    expect(response.headers['content-type']).to.equal('text/html')
    expect(response.body).to.equal('<html><head></head><body>it\'s me</body></html>')
  })

  it('serves approot without slash as index.html', async function () {
    const response = await app.inject({ url: '/appsuite' })
    expect(response.statusCode).to.equal(302)
    expect(response.headers.location).to.equal('/appsuite/')
  })

  it('directly fetches files not referenced in manifest.json files from the upstream servers', async function () {
    const response = await app.inject({ url: '/appsuite/favicon.ico' })
    expect(response.statusCode).to.equal(200)
    expect(response.body).to.equal('not really a favicon, though')
  })

  it('returns 404 if a file misses the app-root', async function () {
    const response = await app.inject({ url: '/example.js' })
    expect(response.statusCode).to.equal(404)
  })
})
