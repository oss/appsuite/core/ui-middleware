/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { injectApp, mockRedis } from './util.js'
import { expect } from 'chai'
import * as td from 'testdouble'
import RedisMock from 'ioredis-mock'

describe('Responses contain custom headers', function () {
  let app, client

  beforeEach(async function () {
    client = (await mockRedis()).client
    await Promise.all([
      client.set('ui-middleware:versionInfo', JSON.stringify({ version: 3215668592 })),
      client.set('ui-middleware:3215668592:/index.html:body', '<html><head></head><body>it\'s me</body></html>'),
      client.set('ui-middleware:3215668592:/index.html:meta', JSON.stringify({ headers: { 'content-type': 'text/html' } })),
      client.set('ui-middleware:3215668592:/index.html.js:body', 'console.log("it\'s me")'),
      client.set('ui-middleware:3215668592:/index.html.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript', dependencies: ['main.css'] } }))
    ])
    app = await injectApp()
  })

  afterEach(async function () {
    await new RedisMock().flushdb()
  })

  after(async function () {
    await app.close()
    td.reset()
  })

  it('index.html has version', async function () {
    const response = await app.inject({ url: '/index.html' })
    expect(response.statusCode).to.equal(200)
    expect(response.headers.version).to.equal('3215668592')
  })

  it('serves requested version', async function () {
    client.set('ui-middleware:123456:/index.html.js:body', 'console.log("it\'s old me")')
    client.set('ui-middleware:123456:/index.html.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript' } }))
    const response = await app.inject({ url: '/index.html.js', headers: { version: '123456' } })
    expect(response.statusCode).to.equal(200)
    expect(response.headers.version).to.equal('123456')
    expect(response.body).to.equal('console.log("it\'s old me")')
    expect(response.headers['latest-version']).to.equal('3215668592')
  })

  it('javascript file contains dependencies', async function () {
    const response = await app.inject({ url: '/index.html.js' })
    expect(response.statusCode).to.equal(200)
    expect(response.body).to.equal('console.log("it\'s me")')
    expect(response.headers.dependencies[0]).to.equal('main.css')
  })
})
