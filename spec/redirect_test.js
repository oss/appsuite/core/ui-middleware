/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { generateSimpleViteManifest, mockConfig, mockFetch, injectApp, mockRedis } from './util.js'
import { expect } from 'chai'
import * as td from 'testdouble'
import RedisMock from 'ioredis-mock'

describe('Redirects', function () {
  let app

  before(async function () {
    await mockConfig({ baseUrls: ['http://ui-server/'] })
    await mockRedis()
    mockFetch({
      'http://ui-server': {
        '/manifest.json': generateSimpleViteManifest({ 'example.js': 'test' }),
        '/example.js': ''
      }
    })
    app = await injectApp()
  })

  afterEach(async function () {
    await new RedisMock().flushdb()
  })

  after(function () {
    td.reset()
  })

  it('without requested location', async function () {
    const response = await app.inject({ method: 'POST', url: '/redirect' })
    expect(response.statusCode).to.equal(302)
    expect(response.headers.location).to.equal('../busy.html')
  })

  it('with requested location', async function () {
    const response = await app.inject({
      method: 'POST',
      url: '/redirect',
      payload: 'location=/appsuite/whatever/path',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
    expect(response.statusCode).to.equal(302)
    expect(response.headers.location).to.equal('/appsuite/whatever/path')
  })

  it('redirects /ui to /', async function () {
    const response = await app.inject({ url: '/ui' })
    expect(response.statusCode).to.equal(302)
    expect(response.headers.location).to.equal('/')
  })
})
