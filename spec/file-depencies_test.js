/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { injectApp, mockRedis } from './util.js'
import { expect } from 'chai'
import * as td from 'testdouble'
import RedisMock from 'ioredis-mock'

describe('JS files with dependencies contain events', function () {
  let app
  let client

  beforeEach(async function () {
    client = (await mockRedis()).client
    client.set('ui-middleware:versionInfo', JSON.stringify({ version: '123456' }))
    client.set('ui-middleware:123456:/example.js:body', 'this is example')
    client.set('ui-middleware:123456:/example.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript' } }))
    client.set('ui-middleware:123456:/index.html.js:body', 'console.log("this is index.html.js")')
    client.set('ui-middleware:123456:/index.html.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript', dependencies: ['main.css'] } }))
    client.set('ui-middleware:123456:/index.html:body', '<html><head></head><body>it\'s me</body></html>')
    client.set('ui-middleware:123456:/index.html:meta', JSON.stringify({ headers: { 'content-type': 'text/html' } }))
    client.set('ui-middleware:123456:/main.css:body', '.foo { color: #000; }')
    client.set('ui-middleware:123456:/main.css:meta', JSON.stringify({ headers: { 'content-type': 'text/css' } }))
    app = await injectApp()
  })

  afterEach(async function () {
    await new RedisMock().flushdb()
    await app.close()
    td.reset()
  })

  it('javascript files from different versions have correct dependencies', async function () {
    const r1 = await app.inject({ url: '/index.html.js' })
    expect(r1.headers.dependencies[0]).to.equal('main.css')

    // client.set('ui-middleware:versionInfo', JSON.stringify({ version: '123457' }))
    client.set('ui-middleware:123457:/index.html.js:body', 'console.log("this is index.html.js")')
    client.set('ui-middleware:123457:/index.html.js:meta', JSON.stringify({ headers: { 'content-type': 'application/javascript', dependencies: ['other.css'] } }))

    const r2 = await app.inject({ url: '/index.html.js', headers: { version: '123457' } })
    expect(r2.headers.dependencies[0]).to.equal('other.css')

    console.log('r1.headers.version', r1.headers.version)
    console.log('r2.headers.version', r2.headers.version)
    const r3 = await app.inject({
      url: '/index.html.js',
      headers: { version: r1.headers.version }
    })
    expect(r3.headers.dependencies[0]).to.equal('main.css')

    const r4 = await app.inject({
      url: '/index.html.js',
      headers: { version: r2.headers.version }
    })
    expect(r4.headers.dependencies[0]).to.equal('other.css')
  })
})
